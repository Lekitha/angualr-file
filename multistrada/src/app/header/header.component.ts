import { Component, OnInit} from '@angular/core';
import { PageTitleService } from './page-title.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
   selector: 'app-header',
   templateUrl: './header.component.html',
   styleUrls: ['./header.component.scss'],
})

// export class HeaderComponent implements OnInit {
//     header: string;
//     // today = Date.now();
//    constructor( private pageTitleService: PageTitleService) {
//     }
//    ngOnInit() {
//     this.pageTitleService.title.subscribe((val: string) => {
//         this.header = val;
        
//     });
  
// }
// }
export class HeaderComponent implements OnInit {
   header: string;
    private date;
   constructor(private  pageTitleService: PageTitleService, private translate: TranslateService) { 
    translate.setDefaultLang('en');
   }
  
   switchLanguage(language: string) {
    this.translate.use(language);
  }
 
   ngOnInit(): void {
    this.pageTitleService.title.subscribe((val: string) => {
              this.header = val;
              
          });
          
    setInterval(()=>{
      this.date = new Date();
    },1)

   }
    // today = Date.now();

   /* private _clockSubscription: Subscription;
    time: Date;
  
    constructor(private clockService: ClockService,private  pageTitleService: PageTitleService) { }
  
    ngOnInit(): void {
      this._clockSubscription = this.clockService.getClock().subscribe(time => this.time = time);
      
      this.pageTitleService.title.subscribe((val: string) => {
               this.header = val;
           });
    }
  
    ngOnDestroy(): void {
      this._clockSubscription.unsubscribe();
    }*/
  
  }
