import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenancecallComponent } from './maintenancecall.component';

describe('MaintenancecallComponent', () => {
  let component: MaintenancecallComponent;
  let fixture: ComponentFixture<MaintenancecallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MaintenancecallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenancecallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
