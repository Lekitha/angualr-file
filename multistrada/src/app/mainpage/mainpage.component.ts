// import { Component, OnInit, ViewChild} from '@angular/core';
// import { FormGroup, FormControl, Validators} from '@angular/forms';

// import {Http, Response, Headers, RequestOptions } from "@angular/http";
// import 'rxjs/add/operator/map';

// @Component({
//    selector: 'app-mainpage',
//    templateUrl: './mainpage.component.html',
//    styleUrls: ['./mainpage.component.css']
// })

// export class MainpageComponent implements OnInit {
//    formdata;
//    cutomerdata;
//    constructor(private http: Http) { }
//    stateCtrl: FormControl;
//    ngOnInit() {
//       this.formdata = new FormGroup({
//          fname: new FormControl("", Validators.compose([
//             Validators.required,
//             Validators.minLength(3)
//          ])),
//          lname: new FormControl("", Validators.compose([
//             Validators.required,
//             Validators.minLength(3)
//          ])),
//          address:new FormControl(""),
//          phoneno:new FormControl("")
//       });
//    }
//    onClickSubmit(data) {
//       document.getElementById("custtable").style.display="";
//       this.cutomerdata = [];
//       for (var prop in data) {
//          this.cutomerdata.push(data[prop]);
//       }
//       console.log(this.cutomerdata);
//    }
// }
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpModule } from '@angular/http';
import { PageTitleService } from '../header/page-title.service'
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})

export class MainpageComponent implements OnInit {
  rows = [];
  rows1 = [];
  rows2 = [];
  rows3 = [];
  rows4 = [];

  constructor(private pageTitleService: PageTitleService, private translate: TranslateService) {
    this.pageTitleService.setTitle("Banbury 2 - Order Start");

    this.singleSelectCheck = this.singleSelectCheck.bind(this);
    this.fetchrunningorders((data) => {
      this.rows = data;

    });
    this.fetchavailableorders((data) => {
      this.rows1 = data;

    });
    this.fetchbomdetails((data) => {
      this.rows2 = data;

    });
    this.fetchunloadmaterial((data) => {
      this.rows3 = data;
    });
    this.fetchmaterialcall((data) => {
      this.rows4 = data;
    });
  }
  switchLanguage(language: string) {
    this.translate.use(language);
  }
  ngOnInit() {
  }


  selected = [];
  Submitdisable: boolean = false;
  table1;
  table2;
  columns4: any[] = [
    { prop: 'DS.No' },
    { prop: 'Material Code' },
    { prop: 'Material Qty' },
  ];
  columns5: any[] = [
    { prop: 'DS.N' },
    { prop: 'Material Code' },
    { prop: 'Pallete Id' },
    { prop: 'Material Qty' }
  ];



  fetchrunningorders(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/running.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetchavailableorders(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/available.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetchbomdetails(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/bom.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetchunloadmaterial(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/unload.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  fetchmaterialcall(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/materialcall.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  onSelect1({ selected,table1}) {
   
    console.log('Select Event', selected, this.selected);
   
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if(table1 == table1){

      this.Submitdisable = true;

    }
  
  }
  
  onSelect2({ selected,table2}) {
   
    console.log('Select Event', selected, this.selected);
   
    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
    if(table2 == table2){

      this.Submitdisable = false;

    }
  
  }
  singleSelectCheck(row: any) {
    return this.selected = [];
  }
  onActivate(event) {
    console.log('Activate Event', event);
  }
  myfun() {
    this.selected = [];
  }
}