import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderinprogressComponent } from './orderinprogress.component';

describe('OrderinprogressComponent', () => {
  let component: OrderinprogressComponent;
  let fixture: ComponentFixture<OrderinprogressComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderinprogressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderinprogressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
