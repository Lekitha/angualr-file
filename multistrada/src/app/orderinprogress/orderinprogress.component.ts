// import { Component, OnInit, ViewChild} from '@angular/core';
// import { FormGroup, FormControl, Validators} from '@angular/forms';

// import {Http, Response, Headers, RequestOptions } from "@angular/http";
// import 'rxjs/add/operator/map';

// @Component({
//    selector: 'app-orderinprogress',
//    templateUrl: './orderinprogress.component.html',
//    styleUrls: ['./orderinprogress.component.css']
// })

// export class OrderInProgress implements OnInit {
//    formdata;
//    cutomerdata;
//    constructor(private http: Http) { }
//    stateCtrl: FormControl;
//    ngOnInit() {
    
//    }

// }
import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { PageTitleService } from '../header/page-title.service'

@Component({
    selector: 'app-orderinprogress',
       templateUrl: './orderinprogress.component.html',
       styleUrls: ['./orderinprogress.component.scss']
})
// export class OrderInProgress implements OnInit {
//    constructor() {}
//    ngOnInit() { }
// }

export class OrderinprogressComponent {
    siteTitle="Banbury 2 - Order Start";
    rows = [];
    rows1 = [];

    selected = [];
    // columns: any[] = [
    //   { prop: 'D.S.N', sortable: false} , 
    //   { prop: 'RecipeCode', sortable: false },
    //   { prop: 'orderno', sortable: false }, 

    
      
    // ];
    // columns1: any[] = [
    //   { prop: 'DS.NO', sortable: false} , 
    //   { prop: 'orderno', sortable: false },

    //   { prop: 'RecipeCode', sortable: false },

    //   { prop: 'No Of Batch',sortable: false },
      
      
    // ];
 
   
    constructor(private pageTitleService: PageTitleService) {
      this.pageTitleService.setTitle("Banbury 2- Order Details");
this.singleSelectCheck = this.singleSelectCheck.bind(this);
      this.fetch((data) => {
        this.rows = data;
      });
      this.fetch1((data) => {
        this.rows1 = data;
      });
   
    }
  
    fetch(cb) {
      const req = new XMLHttpRequest();
      req.open('GET', `assets/data/inprogress.json`);
  
      req.onload = () => {
        cb(JSON.parse(req.response));
      };
  
      req.send();
    }
    fetch1(cb) {
      const req = new XMLHttpRequest();
      req.open('GET', `assets/data/hold.json`);
  
      req.onload = () => {
        cb(JSON.parse(req.response));
      };
  
      req.send();
    }
  
    onSelect({ selected }) {
      console.log('Select Event', selected, this.selected);
  
      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
    }
  
    onActivate(event) {
      console.log('Activate Event', event);
    }
    singleSelectCheck(row:any){
      return this.selected=[];
    }
  }
