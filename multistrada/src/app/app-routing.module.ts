import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserloginComponent } from './userlogin/userlogin.component';
import { MaintenancecallComponent } from './maintenancecall/maintenancecall.component';
import { OrdercompleteComponent } from './ordercomplete/ordercomplete.component';
import { OrderinprogressComponent } from './orderinprogress/orderinprogress.component';
import { MainpageComponent } from './mainpage/mainpage.component';

const routes: Routes = [
  {
    path: '',
    component: UserloginComponent
  },
  {
    path:'app-mainpage',
    component:MainpageComponent
  },
  {
    path: 'app-orderinprogress',
    component: OrderinprogressComponent
  },
  {
    path:'app-maintenancecall',
    component:MaintenancecallComponent
  },
  {
    path:'app-ordercomplete',
    component:OrdercompleteComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
