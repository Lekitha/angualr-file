import { Component, OnInit } from '@angular/core';
import { HttpModule } from '@angular/http';
import {PageTitleService} from '../header/page-title.service'
@Component({
  selector: 'app-ordercomplete',
  templateUrl: './ordercomplete.component.html',
  styleUrls: ['./ordercomplete.component.scss']
})
export class OrdercompleteComponent  {
rows=[];
 
selected = [];

  
  columns:any[]=[
    {
      prop:'DS.No', sortable: false},
      {prop:'RecipeCode',sortable: false} ,
      {prop:'Description',sortable: false} ,
      {prop:'Total Batch (Planned)',sortable: false},
      {prop:'Total Batch (Produced)',sortable: false},
      {prop:'Order Start Time',sortable: false},

      {prop:'Order End Time',sortable: false},


    
  ]
  constructor(private pageTitleService:PageTitleService) { 
    this.pageTitleService.setTitle("Banbury 2 - Order Completion")
    this.fetch((data) => {
      this.rows = data;

    });
   
  }
  fetch(cb) {
    const req = new XMLHttpRequest();
    req.open('GET', `assets/data/complete.json`);

    req.onload = () => {
      cb(JSON.parse(req.response));
    };

    req.send();
  }
  onSelect({ selected }) {
    console.log('Select Event', selected, this.selected);

    this.selected.splice(0, this.selected.length);
    this.selected.push(...selected);
  }
 
  onActivate(event) {
    console.log('Activate Event', event);
  }
}
